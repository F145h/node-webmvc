String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, n) {
        return args[n] ? args[n] : m;
    });
};

global.basePath = __dirname;

if (process.argv.length!=3)
{
    console.log('Error: Invalid arguments.\nUse: node ./nodemvc.js <domain>');
    console.log('Example: node ./nodemvc.js www.site.com');
    return;
}

global.host = process.argv[2];

require(global.basePath + "/hosts/" + global.host + "/config.js");

global.applicationPath = global.basePath + "/hosts/" + global.host + "/webapp/";
global.staticPath = global.basePath + "/hosts/" + global.host + "/static";

g_maxFormContentSize = 64*1024;
g_maxMultipartFieldContentSize = 256*1024;
g_maxMultipartFields = 10;

global.pages = {};
global.controllers = {};
global.models = {};
global.ctrls = {};

global.fs = require('fs');
global.vm = require('vm');
global.querystring = require('querystring');
global.crypto = require('crypto');
global.util = require('util');
global.url = require('url')

global.multipart_parser = require(global.basePath + '/src/external/node-multipart-parser/node-multipart-parser.js');

global.nodemvccore = require(global.basePath + '/src/nodemvccore.js');
global.nodemvccore.start();