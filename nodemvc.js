String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (m, n) {
        return args[n] ? args[n] : m;
    });
};

global.basePath = __dirname;

if (process.argv.length!=3)
{
    console.log('Error: Invalid arguments.\nUse: node ./nodemvc.js <domain>');
    console.log('Example: node ./nodemvc.js www.site.com');
    return;
}

global.host = process.argv[2];

require(global.basePath + "/hosts/" + global.host + "/config.js");

global.applicationPath = global.basePath + "/hosts/" + global.host + "/webapp/";
global.staticPath = global.basePath + "/hosts/" + global.host + "/static";

g_maxFormContentSize = 64*1024;
g_maxMultipartFieldContentSize = 256*1024;
g_maxMultipartFields = 10;

global.pages = {};
global.controllers = {};
global.models = {};
global.ctrls = {};

global.fs = require('fs');
global.vm = require('vm');
global.querystring = require('querystring');
global.crypto = require('crypto');
global.util = require('util');
global.url = require('url')

global.multipart_parser = new function () {    var module = {};    module.exports = {};
function read_header_name(name) {
    console.log("[read_header_name: " + name + "]");
}

function read_header_value(value) {
    console.log("[read_header_value: " + value + "]");
}

function read_data(data) {
    console.log("read_data: " + data);
}

var LF = "\x0a";
var CR = "\x0d";

module.exports.callbacks = function () {
    this.on_header_field = null;
    this.on_header_value = null;
    this.on_part_data = null;

    this.on_part_data_begin = null;
    this.on_headers_complete = null;
    this.on_part_data_end = null;
    this.on_body_end = null;
};

module.exports.parser = function (boundary, settings) {
    this.data = {};
    this.index = 0;
    this.state = 2 /* s_start */;
    this.currentHeaders = {};
    this.currentHeaderName = String();
    this.currentHeaderValue = String();

    this.settings = settings;

    this.lookbehind = new Array(16);

    this.boundary_length = boundary.length;
    this.multipart_boundary = boundary;

    this.execute = function (buf, len) {
        var i = 0;
        var mark = 0;
        var c;
        var cl;
        var is_last = false;

        if (len == undefined)
            len = buf.length;

        while (i < len) {
            c = buf[i];
            is_last = (i == (len - 1));
            switch (this.state) {
                case 2/* s_start */:
                    this.index = 0;
                    this.state = 3 /* s_start_boundary */;

                case 3/* s_start_boundary */:
                    if (this.index == this.boundary_length) {
                        if (c != CR) {
                            return i;
                        }
                        this.index++;
                        break;
                    } else if (this.index == (this.boundary_length + 1)) {
                        if (c != LF) {
                            return i;
                        }
                        this.index = 0;
                        if (this.settings.on_part_data_begin != null)
                            this.settings.on_part_data_begin();
                        this.currentHeaders = {};
                        this.state = 4 /* s_header_field_start */;
                        break;
                    }
                    if (c != this.multipart_boundary[this.index]) {
                        return i;
                    }
                    this.index++;
                    break;

                case 4/* s_header_field_start */:
                    mark = i;
                    this.state = 5 /* s_header_field */;

                case 5/* s_header_field */:
                    if (c == CR) {
                        this.state = 6 /* s_headers_almost_done */;
                        break;
                    }

                    if (c == ':') {
                        var headerFieldName = buf.substring(mark, i);
                        if (this.settings.on_header_field != null)
                            this.settings.on_header_field(headerFieldName);
                        this.currentHeaderName += headerFieldName
                        this.state = 7 /* s_header_value_start */;
                        break;
                    }

                    cl = c.toLowerCase();
                    if ((c != '-') && (cl < 'a' || cl > 'z')) {
                        return i;
                    }
                    if (is_last) {
                        var headerFieldName = (buf.substring(mark, i + 1));
                        if (this.settings.on_header_field != null)
                            this.settings.on_header_field(headerFieldName);
                        this.currentHeaderName += headerFieldName
                    }
                    break;

                case 6/* s_headers_almost_done */:
                    if (c != LF) {
                        return i;
                    }

                    this.state = 10 /* s_part_data_start */;
                    break;

                case 7/* s_header_value_start */:
                    if (c == ' ') {
                        break;
                    }

                    mark = i;
                    this.state = 8 /* s_header_value */;

                case 8/* s_header_value */:
                    if (c == CR) {
                        var headerFieldValue = buf.substring(mark, i)
                        if (this.settings.on_header_value != null)
                            this.settings.on_header_value(headerFieldValue);
                        this.currentHeaderValue += headerFieldValue
                        this.state = 9 /* s_header_value_almost_done */;
                        break;
                    }
                    if (is_last) {
                        var headerFieldValue = buf.substring(mark, (i + 1))
                        if (this.settings.on_header_value != null)
                            this.settings.on_header_value(headerFieldValue);
                        this.currentHeaderValue += headerFieldValue
                    }
                    break;

                case 9/* s_header_value_almost_done */:
                    if (c != LF) {
                        return i;
                    }
                    if (this.currentHeaderName.length > 0)
                        this.currentHeaders[this.currentHeaderName] = this.currentHeaderValue;
                    this.currentHeaderName = String();
                    this.currentHeaderValue = String();
                    this.state = 4 /* s_header_field_start */;
                    break;

                case 10/* s_part_data_start */:
                    var contentName;
                    if ("Content-Disposition" in this.currentHeaders) {
                        var splittedContent = this.currentHeaders["Content-Disposition"].split(";");
                        for (var pc in splittedContent) {
                            var splittedPair = splittedContent[pc].trim().split("=")
                            if (splittedPair.length == 2 && splittedPair[0].trim() == "name") {
                                var nameValue = splittedPair[1].trim();
                                if (nameValue.length > 2 && nameValue[0] == nameValue[nameValue.length - 1] && (nameValue[0] == "'" || nameValue[0] == "\""))
                                    contentName = nameValue.substring(1, nameValue.length - 1);
                                else
                                    contentName = nameValue;
                            }
                        }
                    }
                    if (this.settings.on_headers_complete != null)
                        this.settings.on_headers_complete(contentName, this.currentHeaders);
                    mark = i;
                    this.state = 11 /* s_part_data */;

                case 11/* s_part_data */:
                    if (c == CR) {
                        if (this.settings.on_part_data != null)
                            this.settings.on_part_data(buf.substring(mark, i));
                        mark = i;
                        this.state = 12 /* s_part_data_almost_boundary */;
                        this.lookbehind[0] = CR;
                        break;
                    }
                    if (is_last) {
                        if (this.settings.on_part_data != null)
                            this.settings.on_part_data(buf.substring(mark, (i + 1)));
                    }
                    break;

                case 12/* s_part_data_almost_boundary */:
                    if (c == LF) {
                        this.state = 13 /* s_part_data_boundary */;
                        this.lookbehind[1] = LF;
                        this.index = 0;
                        break;
                    }
                    if (this.settings.on_part_data != null)
                        this.settings.on_part_data(this.lookbehind);
                    this.state = 11 /* s_part_data */;
                    mark = i--;
                    break;

                case 13/* s_part_data_boundary */:
                    if (this.multipart_boundary[this.index] != c) {
                        if (this.settings.on_part_data != null)
                            this.settings.on_part_data(this.lookbehind.join("").substring(0, 2 + this.index));
                        this.state = 11 /* s_part_data */;
                        mark = i--;
                        break;
                    }
                    this.lookbehind[2 + this.index] = c;
                    if ((++this.index) == this.boundary_length) {
                        if (this.settings.on_part_data_end != null)
                            this.settings.on_part_data_end();
                        this.state = 14 /* s_part_data_almost_end */;
                    }
                    break;

                case 14/* s_part_data_almost_end */:
                    if (c == '-') {
                        this.state = 16 /* s_part_data_final_hyphen */;
                        break;
                    }
                    if (c == CR) {
                        this.state = 15 /* s_part_data_end */;
                        break;
                    }
                    return i;

                case 16/* s_part_data_final_hyphen */:
                    if (c == '-') {
                        if (this.settings.on_body_end != null)
                            this.settings.on_body_end();
                        this.state = 17 /* s_end */;
                        break;
                    }
                    return i;

                case 15/* s_part_data_end */:
                    if (c == LF) {
                        this.state = 4 /* s_header_field_start */;
                        this.currentHeaders = {};
                        if (this.settings.on_part_data_begin != null)
                            this.settings.on_part_data_begin();
                        break;
                    }
                    return i;

                case 17/* s_end */:
                    break;

                default:
                    return 0;
            }
            ++i;
        }

        return len;
    };
};;   return module.exports;}();

global.nodemvccore = new function () {    var module = {};    module.exports = {};
module.exports.updateHost = function()
{
    global.hostParser.reloadServices();
    global.hostParser.reloadViews();
    global.hostParser.reloadControllers();
    global.hostParser.reloadModels();

    if (global.application.webapp!=undefined && global.application.webapp.applicationStart!=undefined)
        global.application.webapp.applicationStart();
}

module.exports.start = function(dir)
{
  global.request = new function () {    var module = {};    module.exports = {};function HttpRequest(llReq)
{
    this.method = llReq.method;

    var urlObj = global.url.parse(llReq.url.toString());

    this.path = decodeURIComponent(urlObj.pathname);

    this.queryString = new String();
    if (urlObj.query!=null)
        this.queryString = decodeURIComponent(urlObj.query);

    this.referer = null;

    this.getParameters = {};
    this.postParameters = {};
    this.content = new String();

    this.headers = llReq.headers;

    this.multipartFields = {};
    this.multipartFiles = {};
    this.multipartContent = (("content-type" in this.headers) && this.headers["content-type"].indexOf("multipart")!=-1);

    if ("referer" in this.headers)
        this.referer = this.headers["referer"];

    this.parse = function()
    {
        this.getParameters = global.querystring.parse(this.queryString)
        if (this.method=="POST")
        {
            if (this.multipartContent)
            {
               for(var indxFields in this.multipartFields)
                   this.postParameters[indxFields] = this.multipartFields[indxFields];

               for(var indxFiles in this.multipartFiles)
                   this.postParameters[indxFiles] = this.multipartFiles[indxFiles];
            }
            else
            {
               var parsedParams = global.querystring.parse(this.content)
               for ( var k in parsedParams)
                   this.postParameters[k] = parsedParams[k].toString();
            }
        }
    }

    this.setPath = function(path)
    {
        this.path = path;
    }
}

function create(llReq)
{
    return new HttpRequest(llReq);
}

module.exports.create = create;;   return module.exports;}();
  global.response = new function () {    var module = {};    module.exports = {};function HttpResponse(llRes)
{
    this.llRes = llRes;
    this.status = 500;
    this.header = {};

    this.setStatus = function(status)
    {
        this.status = status;
    }

    this.addHeader = function(key, value)
    {
        this.header[key] = value;
    }

    this.setContentType = function(value)
    {
        this.addHeader("Content-Type", value);
    }

    this.redirectTo = function(url)
    {
        this.setStatus(302);
        this.addHeader("Location", url);
        this.sendHeader();
        this.end();
    }

    this.sendHeader = function()
    {
        this.llRes.writeHead(this.status, this.header);
    }

    this.sendFileContent = function(path, options)
    {
        global.fs.createReadStream(path, options).pipe(this.llRes);
    }

    this.sendContent = function(content)
    {
        if(content.length==0)
            this.llRes.end();
        else
            this.llRes.end(content);
    }

    this.end = function()
    {
        this.llRes.end();
    }
}

function create(llRes)
{
    return new HttpResponse(llRes);
}

module.exports.create = create;   return module.exports;}();
  global.application = new function () {    var module = {};    module.exports = {};function HttpApplication()
{
    this.webapp = {}    
    this.services = {}

    this.addService = function(name, code)
    {
        this.services[name] = global.vm.runInThisContext(code)
    }
}

module.exports.create = function(){
    return new HttpApplication();
};   return module.exports;}().create();
  global.reqestValidator = new function () {    var module = {};    module.exports = {};module.exports.validate = function(request)
{
  switch (request.method)
  {
    case "GET":
    {
        for (var k in this.args.get)
        {
            var oneReqValue = this.args.get[k];
            if (!(oneReqValue in request.getParameters) || request.getParameters[oneReqValue].length==0)
                return false;
        }
    }
    break;
    case "POST":
    {
        for (var k in this.args.post)
        {
            var oneReqValue = this.args.post[k];
            if (!(oneReqValue in request.postParameters) || request.postParameters[oneReqValue].length==0)
                return false;
        }
    }
    break;
  }

  return true;
};   return module.exports;}();
  global.specialResponseGen = new function () {    var module = {};    module.exports = {};module.exports.makeDirectoryContent = function(request, response, targetPath)
{
    var data = new String();

    data += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
    data += "<html>";
    data += "<head>";
    data += "    <title></title>";
    data += "    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />";
    data += "</head>";
    data += "<body>";
    data += "<div style='background-color: #009900;color: white;padding: 2px 0 2px 25px; margin-bottom: 25px;''><h1>Index of:" + request.Path + "</h1></div>";
    data += "<div style=\"padding-left: 50px;padding-top: 25px;\">";

    var dirContent = global.fs.readdirSync(targetPath);
    data += "<table>";
    for (contentIndex in dirContent)
    {
        var fsname = dirContent[contentIndex];
        var fstats = global.fs.lstatSync(targetPath + fsname)
        if (fstats.isDirectory())
        {
            data += "<tr><td><a href='" + request.Path + fsname + "/'>" + fsname + "</a><td>-</td><td>-</td><td>Folder</td><td></td></tr>";
        }
        else if (fstats.isFile()) {
            data += "<tr><td><a href='" + request.Path + fsname + "'>" + fsname + "</a><td>File</td><td>" + fstats.size + "</td><td>File</td></td></tr>";
        }
    }
    data += "</table>";

    data += "</div>";
    data += "</body>";
    data += "</html>";

    response.setStatus(200);
    response.setContentType("text/html");
    response.sendHeader();
    response.sendContent(data);

    return ;
};

function sendErrorPage(response, httpStatus, caption, description)
{
    var data =
    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
    "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
    "<head>" +
    "  <title></title>" +
    "  <style>" +
    "  *{margin:0;padding:0;color:#424242;}" +
    "  .errblock {padding: 25px; overflow: hidden;}" +
    "  .errlogo {width: 90px; height: 110px;line-height:0px;border: 1px solid #424242; float: left;}" +
    "  .errmsg {margin-left: 150px;min-height: 110px;}" +
    "  .wpix{background-color: white;width: 10px;height: 10px; display:inline-block;}" +
    "  .bpix{background-color: #424242;width: 10px;height: 10px; display:inline-block;}" +
    "  .productmsg {padding:10px;}" +
    "  </style>" +
    "</head>" +
    "<body>" +
    "<div class='errblock'>" +
    "  <div class='errlogo'>" +
    "    <p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p>" +
    "  </div>" +
    "  <div class='errmsg'>" +
    "    <h2>" + httpStatus + " " + caption + "</h2>" +
    "    <h4>" + description + "</h4>" +
    "  </div>" +
    "</div>" +
    "<hr />" +
    "<div class='productmsg'>" +
    "NodeMvc - Your freeware and open source MVC Engine for Node.js" +
    "</div>" +
    "</body>" +
    "</html>";

    response.setStatus(httpStatus);

    response.sendHeader();
    response.sendContent(data);
}

module.exports.makeNotFoundPageContent = function(request, response)
{
    sendErrorPage(response, 404, "Not Found", "Path: " + request.Path);
}

module.exports.makeErrorPage = function(request, response, stack)
{
    sendErrorPage(response, 500, "Internal server error", "<div>Path: " + request.Path + "</div><pre>" + stack + "</pre>");
}

module.exports.makeAccessDeniedPageContent = function(request, response)
{
    sendErrorPage(response, 401, "Access denied", "Path: " + request.Path);
}

module.exports.makeInvalidRequestPageContent = function(request, response)
{
    sendErrorPage(response, 400, "Invalid request", "Path: " + request.Path);
};   return module.exports;}();

  global.sessionManager = new function () {    var module = {};    module.exports = {};function Session(inactiveLifeHours)
{
    this.reqTime = new Date().getTime();
    this.userData = {};
    this.inactiveLifeTime = inactiveLifeHours*60*60*1000;
}

function SessionManager()
{

  this.sessions = {};
  this.activeSessionsCount = 0;
  this.sessionKey = 0;

  setInterval(function(sessionManager)
  {
      var ct = new Date().getTime();
      for(var s in sessionManager.sessions)
      {
          var thisSession = sessionManager.sessions[s];
          if (thisSession.reqTime + thisSession.inactiveLifeTime < ct)
          {
              if (global.application.global!=undefined &&  global.application.global.sessionEnd!=undefined)
                  global.application.global.sessionEnd(thisSession);

              --sessionManager.activeSessionsCount;
              delete sessionManager.sessions[s];
          }
      }
  }, 60*1000, this);

  this.isExist = function(key)
  {
      return key in this.sessions;
  }

  this.getData = function(key)
  {
      var session = {}

      if (key in this.sessions)
      {
          session = this.sessions[key];
          session.reqTime = new Date().getTime();
      }

      return session.userData;
  }

  this.create = function(hours)
  {
      var ts = (new Date()).getTime() + " " + ++this.sessionKey;
      var sessionId = global.crypto.createHash('md5').update(ts).digest("hex");

      this.sessions[sessionId] = new Session(hours);

      ++this.activeSessionsCount;

      return sessionId;
  }
}

module.exports.create = function(domain)
{
    return new SessionManager();
};   return module.exports;}().create();
  global.requestManager = new function () {    var module = {};    module.exports = {};function RequestManager()
{
    this.sendPageContent = function(request, response, viewData, session, cookies)
    {
         var content = new String();

         if (request.path in global.pages)
         {
            try
            {
                eval(global.pages[request.path]);
            }
            catch(err)
            {
                response.setStatus(500);
                global.specialResponseGen.makeErrorPage(request, response, err.stack);
                return;
            }
         }

         response.sendHeader();
         response.sendContent(content);
    }

    this.processRequest = function (request, response)
    {
        global.application.routing.route(request);

        var splitedPath = request.path.split('/');

        var controllerName = splitedPath[1];
        var methodName = (splitedPath.length>2)?splitedPath[2]:"index";

        if (controllerName=="controllers" || controllerName=="models" || controllerName=="views")
        {
            global.specialResponseGen.makeAccessDeniedPageContent(request, response);
            return;
        }

        if (controllerName.length==0 || methodName.length==0)
        {
            global.specialResponseGen.makeNotFoundPageContent(request, response);
            return;
        }

         var isService = (controllerName.length>3 && (controllerName.lastIndexOf('.js')==controllerName.length-3));
        if (isService || (controllerName in global.controllers))
        {
            request.parse();

            var sessionkey = new String();

            var application = global.application;

            var session = {};
            var cookies = {};

            var viewData = {};

            if (global.sessionTimeout!=0) {
                if (("cookie" in request.headers)) {
                    var cookieStrValue = request.headers["cookie"];

                    var i, x, y, ARRcookies = cookieStrValue.split(";");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");

                        cookies[x] = y;
                    }

                    if ("NODEMVCSID" in cookies)
                        sessionkey = cookies["NODEMVCSID"];
                }

                if (sessionkey.length == 0 || !global.sessionManager.isExist(sessionkey))
                {
                    var sessionkey = global.sessionManager.create(global.sessionTimeout);
                    response.addHeader("Set-Cookie", "NODEMVCSID=" + sessionkey + ";" + "expires=" + new Date(new Date().getTime() + 1000 * 60 * 60 * global.sessionTimeout).toGMTString() + ";" + "path=/");
                    if (global.application.global!=undefined && global.application.global.sessionStart != undefined)
                        global.application.global.sessionStart(session);
                }

                session = global.sessionManager.getData(sessionkey);
            }

            if (isService)
            {
                var serviceName = controllerName.substring(0, controllerName.length - 3);
                if (serviceName in global.application.services)
                {
                    var svcObj = global.application.services[serviceName];
                    if (methodName in svcObj)
                    {
                        var fn = svcObj[methodName];

                        try
                        {
                            var inObj = request.content.length!=0?JSON.parse(request.content):null;

                            var resultObj = fn(inObj);
                        }

                        catch(err)
                        {
                            response.setStatus(500);
                            global.specialResponseGen.makeErrorPage(request, response, err.stack);
                            return;
                        }

                        response.setStatus(200);
                        response.setContentType("application/json; charset=utf-8");
                        response.sendHeader();
                        response.sendContent(JSON.stringify(resultObj));

                        return;
                    }
                }
            }
            else if (controllerName in global.controllers) {
                
                if (global.application.webapp != undefined && global.application.webapp.masterPageHandler != undefined) {
                    if (global.application.webapp.masterPageHandler(request, response, session, viewData) == false)
                        return;
                }

                var this_controller_definintion = global.controllers[controllerName];

                if (controllerName in global.models)
                {
                    var this_model = global.models[controllerName];

                    var model = null;
                    if (methodName in this_model)
                        model = eval("this_model." + methodName);

                    if (model != null && model.args) {
                        model.validate = global.reqestValidator.validate;
                    }
                }

                var controller = new this_controller_definintion(this, global.application, request, response, viewData, model, session, cookies);

                response.setStatus(200);
                response.setContentType("text/html; charset=utf-8");

                if (methodName in controller)
                {
                    try
                    {
                        var methodResult = false
                        eval("methodResult = controller." + methodName + "();")
                        if (methodResult != false)
                            controller.sendPageContent()
                        else
                            return
                    }

                    catch(err)
                    {
                        response.setStatus(500);
                        global.specialResponseGen.makeErrorPage(request, response, err.stack);
                    }
                    return;
                }
            }
        }
        else
        {
            var targetPath = global.staticPath + request.path;

            if (global.fs.existsSync(targetPath))
            {
                var stat = global.fs.statSync(targetPath);
                if (stat.isFile())
                {
                    var total = stat.size;
                    response.addHeader("Accept-Ranges", "bytes");
                    var date = new Date();
                    date.setDate(date.getDate() + 1);
                    response.addHeader("Expires", date.toUTCString());

                    if (request.headers['range'])
                    {
                        var range = req.headers.range;
                        var parts = range.replace(/bytes=/, "").split("-");
                        var partialstart = parts[0];
                        var partialend = parts[1];

                        var start = parseInt(partialstart, 10);
                        var end = partialend ? parseInt(partialend, 10) : total-1;
                        var chunksize = (end-start)+1;

                        response.setStatus(206);

                        response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + total);
                        response.addHeader("Content-Length", chunksize);

                        response.sendHeader();
                        response.sendFileContent(targetPath, {start: start, end: end});
                    } else {
                        response.setStatus(200);
                        response.addHeader("Content-Length", total);
                        response.sendHeader();
                        response.sendFileContent(targetPath);
                    }
                    return;

                }
            }
        }

        global.specialResponseGen.makeNotFoundPageContent(request, response);
        return;
    }
}

module.exports.create = function()
{
    return new RequestManager()
};   return module.exports;}().create();

  global.updateWatcher = new function () {    var module = {};    module.exports = {};
module.exports.watch = function(callback)
{
    var dir = global.applicationPath;

    var dirs = [];

    var viewsPath = dir + "/views";
    var ctrlsPath = dir + "/controllers";
    var mdlsPath = dir + "/models";

    dirs.push(dir);
    dirs.push(viewsPath);
    dirs.push(ctrlsPath);
    dirs.push(mdlsPath);

    if (global.fs.existsSync(viewsPath))
    {
        var dirViewsContent = global.fs.readdirSync(viewsPath);
        for (contentViewsIndex in dirViewsContent) {
            var controllerName = dirViewsContent[contentViewsIndex];
            var entryFullPath = dir + "/views/" + controllerName;
            stats = global.fs.lstatSync(entryFullPath);
            if (stats.isDirectory()) {
                dirs.push(entryFullPath);
            }
        }
    }

    for(var di in dirs)
    {
      var watchedDirectory = dirs[di];

      if (!global.fs.existsSync(watchedDirectory))
          continue;

      global.fs.watch(watchedDirectory, function (event, filename)
      {
         global.nodemvccore.updateHost();
      }).host = host;
    }
};   return module.exports;}();
  global.hostParser = new function () {    var module = {};    module.exports = {};var webMvcBlockBegin = "<webmvc:";
var webMvcCloseBlockBegin = "</webmvc:";
var webMvcBlockEnd = ">";
var webMvcClosedBlockEnd = " />";

var webMvcBlockContent = "content:";
var webMvcBlockControl = "control:";

var webMvcViewDataBegin = "{$";
var webMvcViewDataEnd = "}";

var webMvcBlockCodeBegin = "{%";
var webMvcBlockCodeEnd = "%}";

function HostParser()
{
    this.makePageProcessor = function(currentPageContent, oneController, pageName)
    {
        var controllerFn = new String();
        var viewFn = new String();

        var page = currentPageContent;

        do {
            var blockBegin = page.indexOf(webMvcViewDataBegin, 0);
            if (blockBegin == -1)
                break;

            var blockEnd = page.indexOf(webMvcViewDataEnd, blockBegin);
            if (blockEnd == -1)
                break;
            blockEnd += webMvcViewDataEnd.length

            var viewDataBlock = page.substr(blockBegin, blockEnd - blockBegin)
            var viewDataValue = viewDataBlock.substr(2, viewDataBlock.length - 3)

            page = page.split(viewDataBlock).join("{% content += viewData['" + viewDataValue + "']; %}")
        }
        while (true);

        do {
            var blockBegin = page.indexOf(webMvcBlockCodeBegin, 0);
            if (blockBegin == -1)
                break;
            var pos = blockBegin;

            pos += webMvcBlockCodeBegin.length;
            var blockEnd = page.indexOf(webMvcBlockCodeEnd, pos);
            if (blockEnd == -1)
                break;

            var codeContent = page.substr(pos, blockEnd - pos);
            var posEnd = blockEnd + webMvcBlockCodeEnd.length;

            viewFn += "content += decodeURIComponent('" + encodeURIComponent(page.substr(0, blockBegin)).split("'").join("\\'") + "');";

            if (codeContent[0] == "=")
                viewFn += "content += " + codeContent.substring(1) + ";";
            else
                viewFn += codeContent;

            page = page.substr(posEnd);
        }
        while (true);

        viewFn += "content += decodeURIComponent('" + encodeURIComponent(page).split("'").join("\\'") + "');";

        if (pageName==="index")
        {
            global.pages["/" + oneController] = viewFn;
        }
        global.pages["/" + oneController + "/" + pageName] = viewFn;
    }

    this.processPage = function(master, oneController, onePage)
    {
        var pageNameLength = onePage.lastIndexOf(".");
        if (pageNameLength == -1 || pageNameLength == 0)
            return;

        var pagePath = global.applicationPath + "views/" + oneController + "/" + onePage;

        var pageName = onePage.substr(0, pageNameLength);

        var pageBlocks = {};

        var pageContent = global.fs.readFileSync(pagePath).toString();

        var pos = 0;
        do
        {
            pos = pageContent.indexOf(webMvcBlockBegin + webMvcBlockContent, pos);
            if (pos == -1)
                break;

            pos += webMvcBlockBegin.length + webMvcBlockContent.length;
            posEnd = pageContent.indexOf(webMvcBlockEnd, pos);
            var fieldName = pageContent.substr(pos, posEnd - pos);
            pos = posEnd + webMvcBlockEnd.length;

            var blockEnd = pageContent.indexOf(webMvcCloseBlockBegin + webMvcBlockContent + fieldName + webMvcBlockEnd, pos);
            var blockContent = pageContent.substr(pos, blockEnd - pos);

            pageBlocks[fieldName] = blockContent;

            pos += fieldName.length;
        }
        while (true);

        var currentPageContent = master;

        for (pageBlocksIndx in pageBlocks)
        {
          currentPageContent = currentPageContent.split(webMvcBlockBegin + webMvcBlockContent + pageBlocksIndx + webMvcClosedBlockEnd).join(pageBlocks[pageBlocksIndx]);
        }

        pageContent = currentPageContent;

        var pageControls = Array();
        pos = 0;
        do
        {
            pos = pageContent.indexOf(webMvcBlockBegin + webMvcBlockControl, pos);
            if (pos == -1)
                break;

            pos += webMvcBlockBegin.length + webMvcBlockControl.length;
            posEnd = pageContent.indexOf(webMvcClosedBlockEnd, pos);
            var fieldName = pageContent.substr(pos, posEnd - pos);
            pos = posEnd + webMvcClosedBlockEnd.length;

            pageControls.push(fieldName);

            pos += fieldName.length;
        }
        while (true);

        for (pageCtrlIndx in pageControls)
        {
          currentPageContent = currentPageContent.split(webMvcBlockBegin + webMvcBlockControl + pageControls[pageCtrlIndx] + webMvcClosedBlockEnd).join(global.ctrls[pageControls[pageCtrlIndx]]);
        }

        this.makePageProcessor(currentPageContent, oneController, pageName);
    }

    this.processController = function(oneController)
    {
        var data = global.fs.readFileSync(global.applicationPath + "controllers/" + oneController);

        var controllerNameEnd = oneController.indexOf(".");
        var controllerName = oneController.substr(0, controllerNameEnd);

        var ctrlClassDef = "var clsDef = function controller_" + controllerName + "(requestManager, application, request, response, viewData, model, session, cookies){ this.requestManager = requestManager; this.application = application; this.request = request; this.response = response; this.viewData = viewData; this.model = model; this.session = session; this.cookies = cookies; this.sendPageContent = function(){  this.requestManager.sendPageContent(this.request, this.response, this.viewData, this.session, this.cookies);}; " + data.toString() + "}"

        global.vm.runInThisContext(ctrlClassDef);

        global.controllers[controllerName] = clsDef;
    }


    this.processModel = function(oneController)
    {
        var data = global.fs.readFileSync(global.applicationPath + "models/" + oneController);

        var controllerNameEnd = oneController.lastIndexOf(".");
        var controllerName = oneController.substr(0, controllerNameEnd);

        var mdlClass = "function model_" + controllerName + "(){" + data.toString() + "}"

        var mdlObj = global.vm.runInThisContext(mdlClass + "; new model_" + controllerName + "();");

        global.models[controllerName] = mdlObj;
    }


    this.reloadServices = function()
    {
        if (global.fs.existsSync(global.applicationPath))
        {
            var dirObjectsContent = global.fs.readdirSync(global.applicationPath);
            for (objIndex in dirObjectsContent)
            {
                var fileObjName = dirObjectsContent[objIndex];
                var entryFullPath = global.applicationPath + "/" + fileObjName
                if (!global.fs.lstatSync(entryFullPath).isDirectory())
                {
                    var fileparts = fileObjName.split('.');
                    if (fileparts.length==2 && fileparts[fileparts.length - 1]=="js")
                    {
                      var fileObjContent = global.fs.readFileSync(global.applicationPath + fileObjName).toString();
                      if (fileObjContent.length!=0) {
                          var moduleName = fileparts[0];
                          var code = "new function(){ " + fileObjContent + " };";
                            
                          if (moduleName == "webapp")
                            global.application.webapp = global.vm.runInThisContext(code)
                          else
                            global.application.addService(moduleName, code);
                      }
                    }
                }
            }
        }
    }

    this.reloadViews = function(){
        var masterFile = global.applicationPath + "views/master.htm";

        var masterpage = new String();

        if (global.fs.existsSync(masterFile))
      	  masterpage = global.fs.readFileSync(masterFile).toString();
        else
          return;

        var viewsDirectory = global.applicationPath + "views";
        var dirViewsContent = global.fs.readdirSync(viewsDirectory);

        for (contentViewsIndex in dirViewsContent)
        {
            var viewFileName = dirViewsContent[contentViewsIndex];
            var entryFullPath = viewsDirectory + "/" + viewFileName;
            if (global.fs.lstatSync(entryFullPath).isFile())
            {
                var fileParts = viewFileName.split(".");
                if (fileParts.length==2 && fileParts[fileParts.length - 1]=="ctrl")
                {
                    var data = global.fs.readFileSync(entryFullPath);
                    global.ctrls[fileParts[0]] = data.toString();
                }
            }
        }

        for (contentViewsIndex in dirViewsContent)
        {
            var viewFileName = dirViewsContent[contentViewsIndex];
            var entryFullPath = viewsDirectory + "/" + viewFileName;
            if (global.fs.lstatSync(entryFullPath).isDirectory())
            {
                var dirPagesContent = global.fs.readdirSync(entryFullPath);
                for (contentPagesIndex in dirPagesContent)
                {
                    var pageName = dirPagesContent[contentPagesIndex];
                    this.processPage(masterpage, viewFileName, pageName);
                }
            }
        }
    }

    this.reloadControllers = function(){
        var controllersDirectory = global.applicationPath + "controllers";
        var dirControllesContent = global.fs.readdirSync(controllersDirectory);
        for (contentCtrlsIndex in dirControllesContent)
        {
            var ctrlrFile = dirControllesContent[contentCtrlsIndex];
            var entryFullPath = controllersDirectory + "/" + ctrlrFile;
            var stats = global.fs.lstatSync(entryFullPath);
            if (stats.isFile())
            {
                this.processController(ctrlrFile);
            }
        }
    }

    this.reloadModels = function() {
        var modelsDirectory = global.applicationPath + "models";
        var dirModelsContent = global.fs.readdirSync(modelsDirectory);
        for (contentModelsIndex in dirModelsContent)
        {
            var mdlFile = dirModelsContent[contentModelsIndex];
            var entryFullPath = modelsDirectory + "/" + mdlFile;
            var stats = global.fs.lstatSync(entryFullPath);
            if (stats.isFile())
            {
                this.processModel(mdlFile);
            }
        }
    }
}


module.exports.create = function(){
   return new HostParser()
};   return module.exports;}().create();

  global.application.routing = new function () {    var module = {};    module.exports = {};function Routing()
{
    this.routeMap = {};

    this.addRoute = function(url, routeTo)
    {
        this.routeMap[url] = routeTo;
    }

    this.route = function(request)
    {
        if (request.path in this.routeMap)
            request.path = this.routeMap[request.path];
    }
}

module.exports.create = function()
{
    return new Routing();
};   return module.exports;}().create();

  module.exports.updateHost();

  global.updateWatcher.watch();

  new function () {    var module = {};    module.exports = {};module.exports.run = function()
{

  var httpHandler = function (req, res)
  {
      var request = global.request.create(req);
      var response = global.response.create(res);

      switch (request.method)
      {
        case "GET":
        {
          global.requestManager.processRequest(request, response);
        }
        break;
        case "POST":
        {
            if (request.multipartContent)
            {
                var form = new global.multiparty.Form({uploadDir: global.basePath+"/tmp", maxFieldSize: g_maxMultipartFieldContentSize, maxFields: g_maxMultipartFields});

                form.parse(req, function(err, fields, files)
                {
                    if(err)
                    {
                      response.end();
                      return;
                    }

                    request.multipartContent = true;
                    request.multipartFields = fields;
                    request.multipartFiles = files;

                    global.requestManager.processRequest(request, response);

                    return;
                });
            }
            else
            {
                req.on('data', function (data)
                {
                    if (request.content.length + data.length > g_maxFormContentSize)
                    {
                        response.end();
                    }
                    request.content += data;
                });

                req.on('end', function ()
                {
                    global.requestManager.processRequest(request, response);
                });
            }
        }
        break;
        default:
          response.setStatus(405);
          response.sendHeader();
          response.sendContent("Method Not Allowed");
          response.end();
      }



  };

  require('http').createServer(httpHandler).listen(global.bindPort);

  console.log(Date() + "\t" + " Http server running.");
};   return module.exports;}().run();
  process.on('exit', function ()
  {
      if (global.application.webapp!=undefined && global.application.webapp.applicationEnd!=undefined)
          global.application.webapp.applicationEnd(ctx);
  });
};   return module.exports;}();
global.nodemvccore.start();