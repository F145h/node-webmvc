function Session(inactiveLifeHours)
{
    this.reqTime = new Date().getTime();
    this.userData = {};
    this.inactiveLifeTime = inactiveLifeHours*60*60*1000;
}

function SessionManager()
{

  this.sessions = {};
  this.activeSessionsCount = 0;
  this.sessionKey = 0;

  setInterval(function(sessionManager)
  {
      var ct = new Date().getTime();
      for(var s in sessionManager.sessions)
      {
          var thisSession = sessionManager.sessions[s];
          if (thisSession.reqTime + thisSession.inactiveLifeTime < ct)
          {
              if (global.application.global!=undefined &&  global.application.global.sessionEnd!=undefined)
                  global.application.global.sessionEnd(thisSession);

              --sessionManager.activeSessionsCount;
              delete sessionManager.sessions[s];
          }
      }
  }, 60*1000, this);

  this.isExist = function(key)
  {
      return key in this.sessions;
  }

  this.getData = function(key)
  {
      var session = {}

      if (key in this.sessions)
      {
          session = this.sessions[key];
          session.reqTime = new Date().getTime();
      }

      return session.userData;
  }

  this.create = function(hours)
  {
      var ts = (new Date()).getTime() + " " + ++this.sessionKey;
      var sessionId = global.crypto.createHash('md5').update(ts).digest("hex");

      this.sessions[sessionId] = new Session(hours);

      ++this.activeSessionsCount;

      return sessionId;
  }
}

module.exports.create = function(domain)
{
    return new SessionManager();
}