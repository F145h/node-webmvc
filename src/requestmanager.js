function RequestManager()
{
    this.sendPageContent = function(request, response, viewData, session, cookies)
    {
         var content = new String();

         if (request.path in global.pages)
         {
            try
            {
                eval(global.pages[request.path]);
            }
            catch(err)
            {
                response.setStatus(500);
                global.specialResponseGen.makeErrorPage(request, response, err.stack);
                return;
            }
         }

         response.sendHeader();
         response.sendContent(content);
    }

    this.processRequest = function (request, response)
    {
        global.application.routing.route(request);

        var splitedPath = request.path.split('/');

        var controllerName = splitedPath[1];
        var methodName = (splitedPath.length>2)?splitedPath[2]:"index";

        if (controllerName=="controllers" || controllerName=="models" || controllerName=="views")
        {
            global.specialResponseGen.makeAccessDeniedPageContent(request, response);
            return;
        }

        if (controllerName.length==0 || methodName.length==0)
        {
            global.specialResponseGen.makeNotFoundPageContent(request, response);
            return;
        }

         var isService = (controllerName.length>3 && (controllerName.lastIndexOf('.js')==controllerName.length-3));
        if (isService || (controllerName in global.controllers))
        {
            request.parse();

            var sessionkey = new String();

            var application = global.application;

            var session = {};
            var cookies = {};

            var viewData = {};

            if (global.sessionTimeout!=0) {
                if (("cookie" in request.headers)) {
                    var cookieStrValue = request.headers["cookie"];

                    var i, x, y, ARRcookies = cookieStrValue.split(";");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");

                        cookies[x] = y;
                    }

                    if ("NODEMVCSID" in cookies)
                        sessionkey = cookies["NODEMVCSID"];
                }

                if (sessionkey.length == 0 || !global.sessionManager.isExist(sessionkey))
                {
                    var sessionkey = global.sessionManager.create(global.sessionTimeout);
                    response.addHeader("Set-Cookie", "NODEMVCSID=" + sessionkey + ";" + "expires=" + new Date(new Date().getTime() + 1000 * 60 * 60 * global.sessionTimeout).toGMTString() + ";" + "path=/");
                    if (global.application.global!=undefined && global.application.global.sessionStart != undefined)
                        global.application.global.sessionStart(session);
                }

                session = global.sessionManager.getData(sessionkey);
            }

            if (isService)
            {
                var serviceName = controllerName.substring(0, controllerName.length - 3);
                if (serviceName in global.application.services)
                {
                    var svcObj = global.application.services[serviceName];
                    if (methodName in svcObj)
                    {
                        var fn = svcObj[methodName];

                        try
                        {
                            var inObj = request.content.length!=0?JSON.parse(request.content):null;

                            var resultObj = fn(inObj);
                        }

                        catch(err)
                        {
                            response.setStatus(500);
                            global.specialResponseGen.makeErrorPage(request, response, err.stack);
                            return;
                        }

                        response.setStatus(200);
                        response.setContentType("application/json; charset=utf-8");
                        response.sendHeader();
                        response.sendContent(JSON.stringify(resultObj));

                        return;
                    }
                }
            }
            else if (controllerName in global.controllers) {
                
                if (global.application.webapp != undefined && global.application.webapp.masterPageHandler != undefined) {
                    if (global.application.webapp.masterPageHandler(request, response, session, viewData) == false)
                        return;
                }

                var this_controller_definintion = global.controllers[controllerName];

                if (controllerName in global.models)
                {
                    var this_model = global.models[controllerName];

                    var model = null;
                    if (methodName in this_model)
                        model = eval("this_model." + methodName);

                    if (model != null && model.args) {
                        model.validate = global.reqestValidator.validate;
                    }
                }

                var controller = new this_controller_definintion(this, global.application, request, response, viewData, model, session, cookies);

                response.setStatus(200);
                response.setContentType("text/html; charset=utf-8");

                if (methodName in controller)
                {
                    try
                    {
                        var methodResult = false
                        eval("methodResult = controller." + methodName + "();")
                        if (methodResult != false)
                            controller.sendPageContent()
                        else
                            return
                    }

                    catch(err)
                    {
                        response.setStatus(500);
                        global.specialResponseGen.makeErrorPage(request, response, err.stack);
                    }
                    return;
                }
            }
        }
        else
        {
            var targetPath = global.staticPath + request.path;

            if (global.fs.existsSync(targetPath))
            {
                var stat = global.fs.statSync(targetPath);
                if (stat.isFile())
                {
                    var total = stat.size;
                    response.addHeader("Accept-Ranges", "bytes");
                    var date = new Date();
                    date.setDate(date.getDate() + 1);
                    response.addHeader("Expires", date.toUTCString());

                    if (request.headers['range'])
                    {
                        var range = req.headers.range;
                        var parts = range.replace(/bytes=/, "").split("-");
                        var partialstart = parts[0];
                        var partialend = parts[1];

                        var start = parseInt(partialstart, 10);
                        var end = partialend ? parseInt(partialend, 10) : total-1;
                        var chunksize = (end-start)+1;

                        response.setStatus(206);

                        response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + total);
                        response.addHeader("Content-Length", chunksize);

                        response.sendHeader();
                        response.sendFileContent(targetPath, {start: start, end: end});
                    } else {
                        response.setStatus(200);
                        response.addHeader("Content-Length", total);
                        response.sendHeader();
                        response.sendFileContent(targetPath);
                    }
                    return;

                }
            }
        }

        global.specialResponseGen.makeNotFoundPageContent(request, response);
        return;
    }
}

module.exports.create = function()
{
    return new RequestManager()
}