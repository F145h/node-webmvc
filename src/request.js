function HttpRequest(llReq)
{
    this.method = llReq.method;

    var urlObj = global.url.parse(llReq.url.toString());

    this.path = decodeURIComponent(urlObj.pathname);

    this.queryString = new String();
    if (urlObj.query!=null)
        this.queryString = decodeURIComponent(urlObj.query);

    this.referer = null;

    this.getParameters = {};
    this.postParameters = {};
    this.content = new String();

    this.headers = llReq.headers;

    this.multipartFields = {};
    this.multipartFiles = {};
    this.multipartContent = (("content-type" in this.headers) && this.headers["content-type"].indexOf("multipart")!=-1);

    if ("referer" in this.headers)
        this.referer = this.headers["referer"];

    this.parse = function()
    {
        this.getParameters = global.querystring.parse(this.queryString)
        if (this.method=="POST")
        {
            if (this.multipartContent)
            {
               for(var indxFields in this.multipartFields)
                   this.postParameters[indxFields] = this.multipartFields[indxFields];

               for(var indxFiles in this.multipartFiles)
                   this.postParameters[indxFiles] = this.multipartFiles[indxFiles];
            }
            else
            {
               var parsedParams = global.querystring.parse(this.content)
               for ( var k in parsedParams)
                   this.postParameters[k] = parsedParams[k].toString();
            }
        }
    }

    this.setPath = function(path)
    {
        this.path = path;
    }
}

function create(llReq)
{
    return new HttpRequest(llReq);
}

module.exports.create = create;