
module.exports.updateHost = function()
{
    global.hostParser.reloadServices();
    global.hostParser.reloadViews();
    global.hostParser.reloadControllers();
    global.hostParser.reloadModels();

    if (global.application.webapp!=undefined && global.application.webapp.applicationStart!=undefined)
        global.application.webapp.applicationStart();
}

module.exports.start = function(dir)
{
  global.request = require(global.basePath + '/src/request.js');
  global.response = require(global.basePath + '/src/response.js');
  global.application = require(global.basePath + '/src/httpapplication.js').create();
  global.reqestValidator = require(global.basePath + '/src/requestvalidator.js');
  global.specialResponseGen = require(global.basePath + '/src/specialresponsegen.js');

  global.sessionManager = require(global.basePath + '/src/sessionmanager.js').create();
  global.requestManager = require(global.basePath + '/src/requestmanager.js').create();

  global.updateWatcher = require(global.basePath + '/src/updatewatcher.js');
  global.hostParser = require(global.basePath + '/src/hostparser.js').create();

  global.application.routing = require(global.basePath + '/src/routing.js').create();

  module.exports.updateHost();

  global.updateWatcher.watch();

  require(global.basePath + '/src/httpserver.js').run();
  process.on('exit', function ()
  {
      if (global.application.webapp!=undefined && global.application.webapp.applicationEnd!=undefined)
          global.application.webapp.applicationEnd(ctx);
  });
}