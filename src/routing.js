function Routing()
{
    this.routeMap = {};

    this.addRoute = function(url, routeTo)
    {
        this.routeMap[url] = routeTo;
    }

    this.route = function(request)
    {
        if (request.path in this.routeMap)
            request.path = this.routeMap[request.path];
    }
}

module.exports.create = function()
{
    return new Routing();
}