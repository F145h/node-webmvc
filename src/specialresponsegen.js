module.exports.makeDirectoryContent = function(request, response, targetPath)
{
    var data = new String();

    data += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
    data += "<html>";
    data += "<head>";
    data += "    <title></title>";
    data += "    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />";
    data += "</head>";
    data += "<body>";
    data += "<div style='background-color: #009900;color: white;padding: 2px 0 2px 25px; margin-bottom: 25px;''><h1>Index of:" + request.Path + "</h1></div>";
    data += "<div style=\"padding-left: 50px;padding-top: 25px;\">";

    var dirContent = global.fs.readdirSync(targetPath);
    data += "<table>";
    for (contentIndex in dirContent)
    {
        var fsname = dirContent[contentIndex];
        var fstats = global.fs.lstatSync(targetPath + fsname)
        if (fstats.isDirectory())
        {
            data += "<tr><td><a href='" + request.Path + fsname + "/'>" + fsname + "</a><td>-</td><td>-</td><td>Folder</td><td></td></tr>";
        }
        else if (fstats.isFile()) {
            data += "<tr><td><a href='" + request.Path + fsname + "'>" + fsname + "</a><td>File</td><td>" + fstats.size + "</td><td>File</td></td></tr>";
        }
    }
    data += "</table>";

    data += "</div>";
    data += "</body>";
    data += "</html>";

    response.setStatus(200);
    response.setContentType("text/html");
    response.sendHeader();
    response.sendContent(data);

    return ;
};

function sendErrorPage(response, httpStatus, caption, description)
{
    var data =
    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
    "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
    "<head>" +
    "  <title></title>" +
    "  <style>" +
    "  *{margin:0;padding:0;color:#424242;}" +
    "  .errblock {padding: 25px; overflow: hidden;}" +
    "  .errlogo {width: 90px; height: 110px;line-height:0px;border: 1px solid #424242; float: left;}" +
    "  .errmsg {margin-left: 150px;min-height: 110px;}" +
    "  .wpix{background-color: white;width: 10px;height: 10px; display:inline-block;}" +
    "  .bpix{background-color: #424242;width: 10px;height: 10px; display:inline-block;}" +
    "  .productmsg {padding:10px;}" +
    "  </style>" +
    "</head>" +
    "<body>" +
    "<div class='errblock'>" +
    "  <div class='errlogo'>" +
    "    <p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='wpix'></p><p class='bpix'></p>" +
    "    <p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p><p class='bpix'></p>" +
    "  </div>" +
    "  <div class='errmsg'>" +
    "    <h2>" + httpStatus + " " + caption + "</h2>" +
    "    <h4>" + description + "</h4>" +
    "  </div>" +
    "</div>" +
    "<hr />" +
    "<div class='productmsg'>" +
    "NodeMvc - Your freeware and open source MVC Engine for Node.js" +
    "</div>" +
    "</body>" +
    "</html>";

    response.setStatus(httpStatus);

    response.sendHeader();
    response.sendContent(data);
}

module.exports.makeNotFoundPageContent = function(request, response)
{
    sendErrorPage(response, 404, "Not Found", "Path: " + request.Path);
}

module.exports.makeErrorPage = function(request, response, stack)
{
    sendErrorPage(response, 500, "Internal server error", "<div>Path: " + request.Path + "</div><pre>" + stack + "</pre>");
}

module.exports.makeAccessDeniedPageContent = function(request, response)
{
    sendErrorPage(response, 401, "Access denied", "Path: " + request.Path);
}

module.exports.makeInvalidRequestPageContent = function(request, response)
{
    sendErrorPage(response, 400, "Invalid request", "Path: " + request.Path);
}