function HttpApplication()
{
    this.webapp = {}    
    this.services = {}

    this.addService = function(name, code)
    {
        this.services[name] = global.vm.runInThisContext(code)
    }
}

module.exports.create = function(){
    return new HttpApplication();
}