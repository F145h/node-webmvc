module.exports.run = function()
{

  var httpHandler = function (req, res)
  {
      var request = global.request.create(req);
      var response = global.response.create(res);

      switch (request.method)
      {
        case "GET":
        {
          global.requestManager.processRequest(request, response);
        }
        break;
        case "POST":
        {
            if (request.multipartContent)
            {
                var form = new global.multiparty.Form({uploadDir: global.basePath+"/tmp", maxFieldSize: g_maxMultipartFieldContentSize, maxFields: g_maxMultipartFields});

                form.parse(req, function(err, fields, files)
                {
                    if(err)
                    {
                      response.end();
                      return;
                    }

                    request.multipartContent = true;
                    request.multipartFields = fields;
                    request.multipartFiles = files;

                    global.requestManager.processRequest(request, response);

                    return;
                });
            }
            else
            {
                req.on('data', function (data)
                {
                    if (request.content.length + data.length > g_maxFormContentSize)
                    {
                        response.end();
                    }
                    request.content += data;
                });

                req.on('end', function ()
                {
                    global.requestManager.processRequest(request, response);
                });
            }
        }
        break;
        default:
          response.setStatus(405);
          response.sendHeader();
          response.sendContent("Method Not Allowed");
          response.end();
      }



  };

  require('http').createServer(httpHandler).listen(global.bindPort);

  console.log(Date() + "\t" + " Http server running.");
}