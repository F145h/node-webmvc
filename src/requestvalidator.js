module.exports.validate = function(request)
{
  switch (request.method)
  {
    case "GET":
    {
        for (var k in this.args.get)
        {
            var oneReqValue = this.args.get[k];
            if (!(oneReqValue in request.getParameters) || request.getParameters[oneReqValue].length==0)
                return false;
        }
    }
    break;
    case "POST":
    {
        for (var k in this.args.post)
        {
            var oneReqValue = this.args.post[k];
            if (!(oneReqValue in request.postParameters) || request.postParameters[oneReqValue].length==0)
                return false;
        }
    }
    break;
  }

  return true;
}