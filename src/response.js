function HttpResponse(llRes)
{
    this.llRes = llRes;
    this.status = 500;
    this.header = {};

    this.setStatus = function(status)
    {
        this.status = status;
    }

    this.addHeader = function(key, value)
    {
        this.header[key] = value;
    }

    this.setContentType = function(value)
    {
        this.addHeader("Content-Type", value);
    }

    this.redirectTo = function(url)
    {
        this.setStatus(302);
        this.addHeader("Location", url);
        this.sendHeader();
        this.end();
    }

    this.sendHeader = function()
    {
        this.llRes.writeHead(this.status, this.header);
    }

    this.sendFileContent = function(path, options)
    {
        global.fs.createReadStream(path, options).pipe(this.llRes);
    }

    this.sendContent = function(content)
    {
        if(content.length==0)
            this.llRes.end();
        else
            this.llRes.end(content);
    }

    this.end = function()
    {
        this.llRes.end();
    }
}

function create(llRes)
{
    return new HttpResponse(llRes);
}

module.exports.create = create