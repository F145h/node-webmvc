var webMvcBlockBegin = "<webmvc:";
var webMvcCloseBlockBegin = "</webmvc:";
var webMvcBlockEnd = ">";
var webMvcClosedBlockEnd = " />";

var webMvcBlockContent = "content:";
var webMvcBlockControl = "control:";

var webMvcViewDataBegin = "{$";
var webMvcViewDataEnd = "}";

var webMvcBlockCodeBegin = "{%";
var webMvcBlockCodeEnd = "%}";

function HostParser()
{
    this.makePageProcessor = function(currentPageContent, oneController, pageName)
    {
        var controllerFn = new String();
        var viewFn = new String();

        var page = currentPageContent;

        do {
            var blockBegin = page.indexOf(webMvcViewDataBegin, 0);
            if (blockBegin == -1)
                break;

            var blockEnd = page.indexOf(webMvcViewDataEnd, blockBegin);
            if (blockEnd == -1)
                break;
            blockEnd += webMvcViewDataEnd.length

            var viewDataBlock = page.substr(blockBegin, blockEnd - blockBegin)
            var viewDataValue = viewDataBlock.substr(2, viewDataBlock.length - 3)

            page = page.split(viewDataBlock).join("{% content += viewData['" + viewDataValue + "']; %}")
        }
        while (true);

        do {
            var blockBegin = page.indexOf(webMvcBlockCodeBegin, 0);
            if (blockBegin == -1)
                break;
            var pos = blockBegin;

            pos += webMvcBlockCodeBegin.length;
            var blockEnd = page.indexOf(webMvcBlockCodeEnd, pos);
            if (blockEnd == -1)
                break;

            var codeContent = page.substr(pos, blockEnd - pos);
            var posEnd = blockEnd + webMvcBlockCodeEnd.length;

            viewFn += "content += decodeURIComponent('" + encodeURIComponent(page.substr(0, blockBegin)).split("'").join("\\'") + "');";

            if (codeContent[0] == "=")
                viewFn += "content += " + codeContent.substring(1) + ";";
            else
                viewFn += codeContent;

            page = page.substr(posEnd);
        }
        while (true);

        viewFn += "content += decodeURIComponent('" + encodeURIComponent(page).split("'").join("\\'") + "');";

        if (pageName==="index")
        {
            global.pages["/" + oneController] = viewFn;
        }
        global.pages["/" + oneController + "/" + pageName] = viewFn;
    }

    this.processPage = function(master, oneController, onePage)
    {
        var pageNameLength = onePage.lastIndexOf(".");
        if (pageNameLength == -1 || pageNameLength == 0)
            return;

        var pagePath = global.applicationPath + "views/" + oneController + "/" + onePage;

        var pageName = onePage.substr(0, pageNameLength);

        var pageBlocks = {};

        var pageContent = global.fs.readFileSync(pagePath).toString();

        var pos = 0;
        do
        {
            pos = pageContent.indexOf(webMvcBlockBegin + webMvcBlockContent, pos);
            if (pos == -1)
                break;

            pos += webMvcBlockBegin.length + webMvcBlockContent.length;
            posEnd = pageContent.indexOf(webMvcBlockEnd, pos);
            var fieldName = pageContent.substr(pos, posEnd - pos);
            pos = posEnd + webMvcBlockEnd.length;

            var blockEnd = pageContent.indexOf(webMvcCloseBlockBegin + webMvcBlockContent + fieldName + webMvcBlockEnd, pos);
            var blockContent = pageContent.substr(pos, blockEnd - pos);

            pageBlocks[fieldName] = blockContent;

            pos += fieldName.length;
        }
        while (true);

        var currentPageContent = master;

        for (pageBlocksIndx in pageBlocks)
        {
          currentPageContent = currentPageContent.split(webMvcBlockBegin + webMvcBlockContent + pageBlocksIndx + webMvcClosedBlockEnd).join(pageBlocks[pageBlocksIndx]);
        }

        pageContent = currentPageContent;

        var pageControls = Array();
        pos = 0;
        do
        {
            pos = pageContent.indexOf(webMvcBlockBegin + webMvcBlockControl, pos);
            if (pos == -1)
                break;

            pos += webMvcBlockBegin.length + webMvcBlockControl.length;
            posEnd = pageContent.indexOf(webMvcClosedBlockEnd, pos);
            var fieldName = pageContent.substr(pos, posEnd - pos);
            pos = posEnd + webMvcClosedBlockEnd.length;

            pageControls.push(fieldName);

            pos += fieldName.length;
        }
        while (true);

        for (pageCtrlIndx in pageControls)
        {
          currentPageContent = currentPageContent.split(webMvcBlockBegin + webMvcBlockControl + pageControls[pageCtrlIndx] + webMvcClosedBlockEnd).join(global.ctrls[pageControls[pageCtrlIndx]]);
        }

        this.makePageProcessor(currentPageContent, oneController, pageName);
    }

    this.processController = function(oneController)
    {
        var data = global.fs.readFileSync(global.applicationPath + "controllers/" + oneController);

        var controllerNameEnd = oneController.indexOf(".");
        var controllerName = oneController.substr(0, controllerNameEnd);

        var ctrlClassDef = "var clsDef = function controller_" + controllerName + "(requestManager, application, request, response, viewData, model, session, cookies){ this.requestManager = requestManager; this.application = application; this.request = request; this.response = response; this.viewData = viewData; this.model = model; this.session = session; this.cookies = cookies; this.sendPageContent = function(){  this.requestManager.sendPageContent(this.request, this.response, this.viewData, this.session, this.cookies);}; " + data.toString() + "}"

        global.vm.runInThisContext(ctrlClassDef);

        global.controllers[controllerName] = clsDef;
    }


    this.processModel = function(oneController)
    {
        var data = global.fs.readFileSync(global.applicationPath + "models/" + oneController);

        var controllerNameEnd = oneController.lastIndexOf(".");
        var controllerName = oneController.substr(0, controllerNameEnd);

        var mdlClass = "function model_" + controllerName + "(){" + data.toString() + "}"

        var mdlObj = global.vm.runInThisContext(mdlClass + "; new model_" + controllerName + "();");

        global.models[controllerName] = mdlObj;
    }


    this.reloadServices = function()
    {
        if (global.fs.existsSync(global.applicationPath))
        {
            var dirObjectsContent = global.fs.readdirSync(global.applicationPath);
            for (objIndex in dirObjectsContent)
            {
                var fileObjName = dirObjectsContent[objIndex];
                var entryFullPath = global.applicationPath + "/" + fileObjName
                if (!global.fs.lstatSync(entryFullPath).isDirectory())
                {
                    var fileparts = fileObjName.split('.');
                    if (fileparts.length==2 && fileparts[fileparts.length - 1]=="js")
                    {
                      var fileObjContent = global.fs.readFileSync(global.applicationPath + fileObjName).toString();
                      if (fileObjContent.length!=0) {
                          var moduleName = fileparts[0];
                          var code = "new function(){ " + fileObjContent + " };";
                            
                          if (moduleName == "webapp")
                            global.application.webapp = global.vm.runInThisContext(code)
                          else
                            global.application.addService(moduleName, code);
                      }
                    }
                }
            }
        }
    }

    this.reloadViews = function(){
        var masterFile = global.applicationPath + "views/master.htm";

        var masterpage = new String();

        if (global.fs.existsSync(masterFile))
      	  masterpage = global.fs.readFileSync(masterFile).toString();
        else
          return;

        var viewsDirectory = global.applicationPath + "views";
        var dirViewsContent = global.fs.readdirSync(viewsDirectory);

        for (contentViewsIndex in dirViewsContent)
        {
            var viewFileName = dirViewsContent[contentViewsIndex];
            var entryFullPath = viewsDirectory + "/" + viewFileName;
            if (global.fs.lstatSync(entryFullPath).isFile())
            {
                var fileParts = viewFileName.split(".");
                if (fileParts.length==2 && fileParts[fileParts.length - 1]=="ctrl")
                {
                    var data = global.fs.readFileSync(entryFullPath);
                    global.ctrls[fileParts[0]] = data.toString();
                }
            }
        }

        for (contentViewsIndex in dirViewsContent)
        {
            var viewFileName = dirViewsContent[contentViewsIndex];
            var entryFullPath = viewsDirectory + "/" + viewFileName;
            if (global.fs.lstatSync(entryFullPath).isDirectory())
            {
                var dirPagesContent = global.fs.readdirSync(entryFullPath);
                for (contentPagesIndex in dirPagesContent)
                {
                    var pageName = dirPagesContent[contentPagesIndex];
                    this.processPage(masterpage, viewFileName, pageName);
                }
            }
        }
    }

    this.reloadControllers = function(){
        var controllersDirectory = global.applicationPath + "controllers";
        var dirControllesContent = global.fs.readdirSync(controllersDirectory);
        for (contentCtrlsIndex in dirControllesContent)
        {
            var ctrlrFile = dirControllesContent[contentCtrlsIndex];
            var entryFullPath = controllersDirectory + "/" + ctrlrFile;
            var stats = global.fs.lstatSync(entryFullPath);
            if (stats.isFile())
            {
                this.processController(ctrlrFile);
            }
        }
    }

    this.reloadModels = function() {
        var modelsDirectory = global.applicationPath + "models";
        var dirModelsContent = global.fs.readdirSync(modelsDirectory);
        for (contentModelsIndex in dirModelsContent)
        {
            var mdlFile = dirModelsContent[contentModelsIndex];
            var entryFullPath = modelsDirectory + "/" + mdlFile;
            var stats = global.fs.lstatSync(entryFullPath);
            if (stats.isFile())
            {
                this.processModel(mdlFile);
            }
        }
    }
}


module.exports.create = function(){
   return new HostParser()
}