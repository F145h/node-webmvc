
module.exports.watch = function(callback)
{
    var dir = global.applicationPath;

    var dirs = [];

    var viewsPath = dir + "/views";
    var ctrlsPath = dir + "/controllers";
    var mdlsPath = dir + "/models";

    dirs.push(dir);
    dirs.push(viewsPath);
    dirs.push(ctrlsPath);
    dirs.push(mdlsPath);

    if (global.fs.existsSync(viewsPath))
    {
        var dirViewsContent = global.fs.readdirSync(viewsPath);
        for (contentViewsIndex in dirViewsContent) {
            var controllerName = dirViewsContent[contentViewsIndex];
            var entryFullPath = dir + "/views/" + controllerName;
            stats = global.fs.lstatSync(entryFullPath);
            if (stats.isDirectory()) {
                dirs.push(entryFullPath);
            }
        }
    }

    for(var di in dirs)
    {
      var watchedDirectory = dirs[di];

      if (!global.fs.existsSync(watchedDirectory))
          continue;

      global.fs.watch(watchedDirectory, function (event, filename)
      {
         global.nodemvccore.updateHost();
      }).host = host;
    }
}