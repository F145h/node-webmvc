

this.applicationStart = function()
{
    console.log(global.host + ": applicationStart")
    application.routing.addRoute('/', '/home/index');
}

this.sessionStart = function(session)
{
    console.log(global.host + ": sessionStart");

    session["Auth"] = false;
    session["User"] = null;
}

this.masterPageHandler = function(request, response, session, viewData)
{

}

this.sessionEnd = function(session)
{
    console.log(global.host + ": sessionEnd");
}

this.applicationEnd = function()
{
    console.log(global.host + ": applicationStart");
}

